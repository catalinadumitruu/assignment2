function addTokens(input, tokens){

    if(typeof(input) != 'string'){
        throw 'Invalid input';
    }

    if(input.length < 6){
        throw "Input should have at least 6 characters";
    }

    for(var i = 0; i < tokens.length; i++){
        var myVar = tokens[i];
        var val = tokens[i].tokenName;

        if(typeof(val) !== 'string'){
            throw 'Invalid array format';
        }
    }

    var inputVect = input.split(" ");
    var checker = 0;
    
        inputVect.forEach( (word) => {
            if(word === "..."){
                checker = 1;
            }
        });
    
    if(checker == 0){
        console.log(input);
        return input;
    }

    var tobeReplaced = "...";

    for(var x = 0; x < tokens.length; x++){
        if(input.indexOf(tobeReplaced) != -1){
            input = input.replace(tobeReplaced, '${' + tokens[x].tokenName + '}');
        }
    }

    return input;

 }

const app = {
    addTokens: addTokens
}

module.exports = app;